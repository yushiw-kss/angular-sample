# Create Angular Project

## Run the docker container
```bash
$ cd docker
$ docker-compose --project-dir .. -f docker-compose-create.yml up
```

## Create project
```bash
$ docker exec -it ang-node bash
```

### On bash of the docker container
```bash
$ ng new frontend
```

## Run the project

### On bash of the docker container
```bash
$ cd frontend
$ yarn start --host 0.0.0.0 --poll 100
```
